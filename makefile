CC=g++

CFLAGS=-msse3
INCDIR=-I.

PROGRAMS := task1 task2

all: $(PROGRAMS)

task1: task1.cpp
	$(CC) $(CFLAGS) task1.cpp -o task1

task2: task2.cpp
	$(CC) $(CFLAGS) task2.cpp -o task2

clean:
	rm -f *.o $(PROGRAMS)