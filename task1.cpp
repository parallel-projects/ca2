#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include <sys/time.h>
#include "x86intrin.h"
#define        VECTOR_SIZE              1048576
int main (void)
{
    printf("Student IDs:\n810196518\n810196562\n\n");

    float *floatVec;
    floatVec = new float [VECTOR_SIZE];
    float *floats;
    floats = new float [4];
    float *indexes;
    indexes = new float [4];
    srand((unsigned int)time(NULL));
    
    for (int i = 0; i < VECTOR_SIZE; i++) {
        floatVec[i] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/100.0));
    }
    
    struct timeval serial_start, serial_end, parallel_start, parallel_end;
    
    // Serial implementation
    gettimeofday(&serial_start, NULL);
    
    int index = 0;
    float max = 0.0;
    for (int i = 0; i < VECTOR_SIZE; i++){
        float current_element = floatVec[i];
        if (current_element >= max){
            max = current_element;
            index = i;
        }
    }
    
    gettimeofday(&serial_end, NULL);
    printf("Serial result:\n");
    printf("max number is: %f at %dth index\n", max, index);
    long serial_seconds = (serial_end.tv_sec - serial_start.tv_sec);
    long serial_micros = ((serial_seconds * 1000000) + serial_end.tv_usec) - (serial_start.tv_usec);
    printf("serial executaion time is %ld s and %ld micros\n\n", serial_seconds, serial_micros);
    
    // Parallel implementation
    gettimeofday(&parallel_start, NULL);

    float parallel_max;
    float parallel_index = 0;
    __m128 maxval = _mm_setzero_ps();
    __m128 vec;
    __m128i cnt = _mm_setzero_si128();
    __m128 eq_result;
    __m128 neq_result;
    __m128 current_indexes;
    __m128 updated_indexes = _mm_set_ps(3.0, 2.0, 1.0, 0.0);
    for (int i = 0; i < VECTOR_SIZE ; i+=4) {
        current_indexes = _mm_set_ps(float(i+3), float(i+2), float(i+1), float(i));
        vec = _mm_loadu_ps(&floatVec[i]);
        maxval = _mm_max_ps(maxval, vec);
        eq_result = _mm_cmpeq_ps(vec, maxval);
        neq_result = _mm_cmpneq_ps(vec, maxval);
        updated_indexes = _mm_or_ps(_mm_and_ps(eq_result, current_indexes),
                                    _mm_and_ps(neq_result, updated_indexes));
    }
    _mm_store_ps(floats, maxval);
    for (int i = 0; i < 4; i++) {
        maxval = _mm_max_ps(maxval, _mm_shuffle_ps(maxval, maxval, 0x93));
    }
    _mm_store_ss(&parallel_max, maxval);
    _mm_store_ps(indexes, updated_indexes);
    for(int i = 0;i<4;i++){
        if (parallel_max == floats[i] && indexes[i] > parallel_index)
            parallel_index = indexes[i];
    }

    gettimeofday(&parallel_end, NULL);
    printf("Parallel result:\n");
    printf("max number is: %f at %dth index\n", parallel_max, int(parallel_index));
    long parallel_seconds = (parallel_end.tv_sec - parallel_start.tv_sec);
    long parallel_micros = ((parallel_seconds * 1000000) + parallel_end.tv_usec) - (parallel_start.tv_usec);
    printf("parallel executaion time is %ld s and %ld micros\n\n", parallel_seconds, parallel_micros);
    printf("speed up: %f\n\n", (float)serial_micros/(float)parallel_micros);
    return 0;
}