#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include <sys/time.h>
#include <cmath>
#include <x86intrin.h>
#define     VECTOR_SIZE     1048576

int main (void)
{
    printf("Student IDs:\n810196518\n810196562\n\n");

    float *floatVec;    
	floatVec = new float [VECTOR_SIZE];
    srand((unsigned int)time(NULL));

    for(long i = 0; i < VECTOR_SIZE; i++)
        floatVec[i] = float(rand())/float((RAND_MAX)) * 100.0;
    
    struct timeval serialStart, serialEnd, parallelStart, parallelEnd;

    // Serial implementation   
    gettimeofday(&serialStart, NULL);

    float sSum[4] = {0.0, 0.0, 0.0, 0.0};
    float sSumRes, sAverage;

    for(long i = 0; i < VECTOR_SIZE; i+=4)
        sSum[0] += floatVec[i];
    for(long i = 0; i < VECTOR_SIZE; i+=4)
        sSum[1] += floatVec[i + 1];
    for(long i = 0; i < VECTOR_SIZE; i+=4)
        sSum[2] += floatVec[i + 2];
    for(long i = 0; i < VECTOR_SIZE; i+=4)
        sSum[3] += floatVec[i + 3];
    
    sSumRes = (sSum[0] + sSum[1]) + (sSum[2] + sSum[3]);
    sAverage = sSumRes / VECTOR_SIZE;
    
    float sSquareDiff[4] = {0.0, 0.0, 0.0, 0.0};
    float sSquareDiffRes, sVariance, sStandardDeviation;

    for(long i = 0; i < VECTOR_SIZE; i+=4)
        sSquareDiff[0] += (floatVec[i] - sAverage) * (floatVec[i] - sAverage);
    for(long i = 0; i < VECTOR_SIZE; i+=4)
        sSquareDiff[1] += (floatVec[i + 1] - sAverage) * (floatVec[i + 1] - sAverage);
    for(long i = 0; i < VECTOR_SIZE; i+=4)
        sSquareDiff[2] += (floatVec[i + 2] - sAverage) * (floatVec[i + 2] - sAverage);
    for(long i = 0; i < VECTOR_SIZE; i+=4)
        sSquareDiff[3] += (floatVec[i + 3] - sAverage) * (floatVec[i + 3] - sAverage);

    sSquareDiffRes = (sSquareDiff[0] + sSquareDiff[1]) + (sSquareDiff[2] + sSquareDiff[3]);
    sVariance = sSquareDiffRes / VECTOR_SIZE;
    sStandardDeviation = sqrt(sVariance);

    gettimeofday(&serialEnd, NULL);

    printf("Serial result:\n");
    printf("Average is %f\n", sAverage);
    printf("Standard deviation is %f\n", sStandardDeviation);
    long serialSeconds = (serialEnd.tv_sec - serialStart.tv_sec);
    long serialMicros = ((serialSeconds * 1000000) + serialEnd.tv_usec) - (serialStart.tv_usec);
    printf("Serial executaion time %ld s and %ld micros.\n\n", serialSeconds, serialMicros);
    
    // Parallel implementation
    gettimeofday(&parallelStart, NULL);

    __m128 vec;
    __m128 pSum = _mm_set1_ps(0.0f);
    float pSumRes, pAverage;

    for(long i = 0; i < VECTOR_SIZE; i+=4)
    {
        vec = _mm_loadu_ps(&floatVec[i]);
        pSum = _mm_add_ps(pSum, vec);
    }

    pSum = _mm_hadd_ps(pSum, pSum);
    pSum = _mm_hadd_ps(pSum, pSum);
    pSumRes = _mm_cvtss_f32(pSum);
    pAverage = pSumRes / VECTOR_SIZE;
    
    __m128 pAverageVec = _mm_set1_ps(pAverage);
    __m128 pDiff, pSquareDiff;
    __m128 pSumSquareDiff = _mm_set1_ps(0.0f);
    float pSumSquareDiffRes, pVariance, pStandardDeviation;

    for(long i; i < VECTOR_SIZE; i+=4)
    {
        vec = _mm_loadu_ps(&floatVec[i]);
        pDiff = _mm_sub_ps(vec, pAverageVec);
        pSquareDiff = _mm_mul_ps(pDiff, pDiff);
        pSumSquareDiff = _mm_add_ps(pSumSquareDiff, pSquareDiff);
    }

    pSumSquareDiff = _mm_hadd_ps(pSumSquareDiff, pSumSquareDiff);
    pSumSquareDiff = _mm_hadd_ps(pSumSquareDiff, pSumSquareDiff);
    pSumSquareDiffRes = _mm_cvtss_f32(pSumSquareDiff);
    pVariance = pSumSquareDiffRes / VECTOR_SIZE;
    pStandardDeviation = sqrt(pVariance);

    gettimeofday(&parallelEnd, NULL);

    printf("Parallel result:\n");
    printf("Average is %f\n", pAverage);
    printf("Standard deviation is %f\n", pStandardDeviation);
    long parallelSeconds = (parallelEnd.tv_sec - parallelStart.tv_sec);
    long parallelMicros = ((parallelSeconds * 1000000) + parallelEnd.tv_usec) - (parallelStart.tv_usec);
    printf("Parallel executaion time %ld s and %ld micros.\n\n", parallelSeconds, parallelMicros);
    printf("speed up: %f\n\n", (float)serialMicros/(float)parallelMicros);

}